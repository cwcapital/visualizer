//server.js
'use strict'

//dependencies
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors')
var path = require('path')

//instances
var app = express();
var router = express.Router();

//set our port to predetermined or 3010
var port = process.env.API_PORT || 8081;

//configure API to use bodyParser and look for JSON data in the
//request body
//app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'src', 'client')));

app.get('/', function(req, res) {
  res.sendFile(path.resolve(__dirname, 'src', 'client', 'index.html'))
})

//starts the server and listens for requests
app.listen(port, function() {
  console.log('api running on port ' + port);
});
