'use strict'

var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var config = env => {
  if (env.NODE_ENV == 'PRODUCTION') {
      var TICK_SERVER_URL = "https://dynamicdata.colewoodcapital.com";
  } else {
      var TICK_SERVER_URL = "http://localhost:3000";
  }

  class MyPlugin {
    apply(compiler) {
      compiler.plugin("compilation", new webpack.DefinePlugin({
        TICK_SERVER_URL: JSON.stringify(TICK_SERVER_URL),
      }));
    }
  }

  return({
    entry: [
      'react-hot-loader/patch', // RHL patch
      APP_DIR + '/index.js'
    ],
    output: {
      path: BUILD_DIR,
      filename: 'bundle.js'
    },
    devServer: {
      hot: true,
      contentBase: 'src/client',
      host:"localhost",
      port: 8080
    },
    plugins: [
      new MyPlugin()
    ],
    devtool: 'eval-source-map',
    module: {
      loaders: [
        {
          test: /\.jsx?/,
          include: APP_DIR,
          loaders: [ 'babel-loader']
        }, {
          test: /\.html$/,
          loader: "file?name=[name].[ext]"
        }, {
          test: /\.css/,
          loaders: ['style-loader', 'css-loader'],
          include: __dirname + '/src'
        }
      ]
  }
})}

module.exports = config;
