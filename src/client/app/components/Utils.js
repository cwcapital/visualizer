
class Candle {
  constructor(tstamp, tick) {
    this.tstamp = tstamp
    this.open = tick
    this.close = tick
    this.low = tick
    this.high = tick
  }

  update(tick) {
    if (tick > this.high) {
      this.high = tick
    }

    else if (tick < this.low) {
      this.low = tick
    }

    this.close = tick
  }

  render() {
    return [this.tstamp.getTime(), this.open, this.high, this.low, this.close]
  }
}

class RenkoCandle {
  constructor(tstamp, prevCandleDirection, tick) {
    this.tstamp = tstamp
    this.open = tick
    this.close = tick
    this._prev = prevCandleDirection // -1=first_candle, 0=up, 1=down
    this._dir = -1 // 0=up, 1=down
  }

  update(tick, box_size)
  {
    if (this._dir < 0)
    {
     if(this._prev == 0)
     {
       if (tick - this.open >= box_size) {
         this.close = this.open + box_size;
         this._dir = 0;
       }

       if (this.open - tick >= (2 * box_size)) {
         this.open -= box_size;
         this.close = this.open - box_size;
         this._dir = 1;
       }
     }

     else if(this._prev == 1)
     {
       if (tick - this.open >= (2 * box_size)) {
         this.open += box_size;
         this.close = this.open + box_size;
         this._dir = 0;
       }

       if (this.open - tick >= box_size) {
         this.close = this.open - box_size;
         this._dir = 1;
       }
     }

     else // first candle (no previous candle)
     {
       if (tick - this.open >= box_size) {
         this.close = this.open + box_size;
         this._dir = 0;
       }

       if (this.open - tick >= box_size) {
         this.close = this.open - box_size;
         this._dir = 1;
       }
     }
    }

    return (this._dir >= 0);
 }

  render() {
    let o,h,l,c;

    if (this._dir == 0) {
      o = this.open;
      h = this.close;
      l = this.open;
      c = this.close;
    }

    if (this._dir == 1) {
      o = this.open;
      h = this.open;
      l = this.close;
      c = this.close;
    }

    // return [this.tstamp.getTime(), o, h, l, c]
    return [this.tstamp, o, h, l, c]
  }
}

export function ticksToRenko(ti, box_size) {
  if (ti.length < 1) {
    return null;
  }

  // let candle_ts = new Date(ti[0].tstamp);
  const HOUR_IN_MILLISECONDS = 60 * 60 * 1000;
  let candles = [];

  let candle_ts = 1;
  let prev_tstamp = new Date(ti[0].tstamp);
  let candle = new RenkoCandle(candle_ts, -1, ti[0].bid);
  let id_to_tstamp = [{id: candle_ts, tstamp: prev_tstamp.toISOString()}];

  for (let i=1; i<ti.length; i++) {
    let bHasCompleted = candle.update(ti[i].bid, box_size);

    if (bHasCompleted) {
      // add renko candle to candles[] after has completely formed
      candles.push(candle.render())

      // begin new candle
      if (i+1 < ti.length) {
        // candle_ts = new Date(ti[i+1].tstamp);
        candle_ts += 1;

        // double increment if there is a gap >= 1 hour
         let ts = new Date(ti[i+1].tstamp);
         let last_candle_direction, this_candle_open_prc;

         if (ts - prev_tstamp >= HOUR_IN_MILLISECONDS) {
           candle_ts += 1;
           last_candle_direction = candle._dir;
           this_candle_open_prc = candle.close;
         } else {
           last_candle_direction = candle._dir;
           this_candle_open_prc = candle.close;//ti[i+1].bid;
         }

        //----
         id_to_tstamp.push({
           id: candle_ts,
           tstamp: ts.toISOString()
         })

        prev_tstamp = ts;
        candle = new RenkoCandle(candle_ts,
                          last_candle_direction, this_candle_open_prc);
      }
    }
  }

  return [candles, id_to_tstamp];
}

export function ticksToCandles(ti, timeframe) {
  if (ti.length < 1) {
    return null;
  }

  let candle_ts = new Date(ti[0].tstamp)
  let candle = new Candle(candle_ts, ti[0].bid)
  let candles = []

  for (let i=1; i<ti.length; i++) {
    let ts = new Date(ti[i].tstamp)

    if (ts - candle_ts >= (timeframe * 60000)) {
      candles.push(candle.render());
      candle_ts = ts;
      candle = new Candle(candle_ts, ti[i].bid);
    }
    else {
      candle.update(ti[i].bid);
    }
  }

  return candles;
}
