
import React, { Component } from 'react';
import Highcharts from 'highcharts/highstock';
import {
  HighchartsStockChart, Chart, withHighcharts, XAxis, YAxis, Title, Legend,
  CandlestickSeries, SplineSeries, Navigator, RangeSelector, Tooltip
} from 'react-jsx-highstock';
import ExampleCode from '../utils/ExampleCode';
import code from './exampleCode';
import { createRandomData } from '../utils/data-helpers';

class App extends Component {

  constructor (props) {
    super(props);
  }

  render() {
    const data1 = this.props.data;
    return (
      <div className="app">
        <HighchartsStockChart>
          <Chart zoomType="x" />

          <Title>BitMEX Exchange</Title>

          <Legend>
            <Legend.Title>Key</Legend.Title>
          </Legend>

          <Tooltip />

          <XAxis>
            <XAxis.Title>Time</XAxis.Title>
          </XAxis>

          <YAxis id="price">
            <YAxis.Title>Price</YAxis.Title>
            <CandlestickSeries id="profit" name="XBT/USD" data={data1} dataGrouping={{enabled: false}}/>
          </YAxis>

          <Navigator>
            <Navigator.Series seriesId="profit" />
          </Navigator>
        </HighchartsStockChart>

        <ExampleCode name="Highstocks">{code}</ExampleCode>

      </div>
    );
  }
}

export default withHighcharts(App, Highcharts);
