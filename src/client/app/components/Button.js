import React from 'react'
//import { toggleShowSubscriptions } from '../actions/userActions'

export default class Button extends React.Component {
    constructor() {
      super();
      this.state = {
        bPressed: false,
        bClass: "btn btn-default btn-xs btn-center",
        display: "Show"
      }
    }

    handleClick(id, name) {
      console.log(id, " ... ", name)
      let eventObj = {
        name: name,
        pressed: !this.state.bPressed,
        events: []
      };

      this.props.events.forEach((e) => {
        if(e.user__id === id)
        {
          eventObj.events.push({
            log_type: e.log_type,
            symbol: e.symbol
          })
        }
      })

      //--- update state
      if(this.state.bPressed)
        this.setState({bPressed: false, bClass: "btn btn-default btn-xs btn-center", display: "Show"});
      else {
        this.setState({bPressed: true, bClass: "btn btn-success btn-xs btn-center", display: "Hide"});
      }

    //  toggleShowSubscriptions(eventObj)
    }

    render() {
      return(
        <a id={this.props.id} onClick={this.handleClick.bind(this,this.props.id,this.props.name)} class={this.state.bClass}>{this.state.display}</a>
      )
    }
}

Button.propTypes = {
  id: React.PropTypes.number.isRequired,
  name: React.PropTypes.string.isRequired,
  events: React.PropTypes.array.isRequired
  //isPressed: React.PropTypes.bool.isRequired,
  //display: React.PropTypes.string.isRequired
}
