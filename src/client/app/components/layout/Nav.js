import React from "react";
import { IndexLink, Link } from "react-router";

export default class Nav extends React.Component {
  constructor() {
    super()
    this.state = {
      collapsed: true,
      dropped: false,
    };
  }

  toggleCollapse() {
    //const collapsed = !this.state.collapsed;
    //this.setState({collapsed});
  }

  toggleDropdown() {
    //const dropped = !this.state.dropped;
    //this.setState({dropped});
  }

  componentDidMount() {
    //listStore.on("success", this.updateOrderBook)
    //ListActions.getConfig();
    //LogActions.getConfig();
    //initOrderBook();
  }

  componentWillUnmount() {
    //listStore.removeListener("success", this.updateOrderBook)
  }

  render() {
    const { location } = this.props;
    const { collapsed } = this.state;
    const { dropped } = this.state;
    const logsClass = location.pathname.match(/^\/logs/) ? "active" : "";
    //const settingsClass = location.pathname.match(/^\/settings/) ? "active" : "";
    const navClass = collapsed ? "collapse" : "";
    const dropdownClass = dropped ? " collapsed" : "";
    const ariaExpanded = dropped ? "true" : "false;"
    const bNavIsIn = dropped ? " in" : "";
    const bNavIsInStyle = dropped ? "" : "height: 1px";

    return (
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" onClick={this.toggleDropdown.bind(this)} class={"navbar-toggle" + dropdownClass} data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded={ariaExpanded}>
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Evolve Markets</a>
          </div>

          <div class={"navbar-collapse collapse" + bNavIsIn} id="navbar-main" aria-expanded={ariaExpanded} style={{bNavIsInStyle}}>
            <ul class="nav navbar-nav">
              <li class={logsClass}>
                <Link to="logs" onClick={this.toggleCollapse.bind(this)}>Logs</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
