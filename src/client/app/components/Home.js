import React from 'react'
import Chart from './Chart.js'
var Stock = require('highcharts/modules/stock')
import DatePicker from 'react-datepicker/dist/react-datepicker.js'
var Button = require('simple-react-button')
import * as Utils from './Utils.js'
var deepcopy = require('deepcopy')
const axios = require('axios')
import moment from 'moment'
import ReactDataGrid from 'react-data-grid';
var _ = require('lodash')

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

class SortedArray {
  min() {
    return this.x_sorted[0];
  }

  max() {
    return this.x_sorted[this.x_sorted.length - 1];
  }

  insert(val) {
    this.x.push(val);
    let idx = _.sortedIndex(this.x_sorted, val);
    this.x_sorted.splice(idx, 0, val);

    if (this.x.length > this.max_size) {
      let del = this.x.shift();
      let sorted_idx = _.sortedIndex(this.x_sorted, del);
      this.x_sorted.splice(sorted_idx, 1)
    }
  }

  constructor(array, max_size) {
    this.x = array.slice(0)
    this.x_sorted = array.slice(0).sort()
    this.max_size = max_size
  }
}

class Home extends React.Component {
  constructor(props) {
    super(props);
    var end = moment();
    var start = moment(end).subtract(1, 'days');
    this.state = {
      data: [],
      relData: [],
      startDate: start,
      endDate: end,
      timeframe: 15,
      showRelative: false,
      renkoChart: false,
      renkoTimestamps: []
    }
    this.handleStartDateChange = this.handleStartDateChange.bind(this)
    this.handleEndDateChange = this.handleEndDateChange.bind(this)
    this.handleTimeframeChange = this.handleTimeframeChange.bind(this)
    this.renderClick = this.renderClick.bind(this)
    this.predictClick = this.predictClick.bind(this)
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
  }

  handleStartDateChange(date, key) {
    this.setState({
      startDate: date
    })
  }

  handleEndDateChange(date, key) {
    this.setState({
      endDate: date
    })
  }

  handleCheckboxChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleTimeframeChange(evt) {
    this.setState({timeframe: evt.target.value})
  }

  renderClick() {
    let start_date = this.state.startDate.format('YYYY-MM-DD-HH-mm-00')
    console.log(start_date)
    let end_date = this.state.endDate.format('YYYY-MM-DD-HH-mm-00')
    var url = TICK_SERVER_URL + '/api/ticks?exchange=bitmex&symbol=xbtusd&from=' +
                start_date + '&to=' + end_date;

    axios.get(url)
      .then(response => {
        let ticks = response.data.data

        let candles;
        if (this.state.renkoChart) {
          let res = Utils.ticksToRenko(ticks, Number(this.state.timeframe));
          candles = res[0];
          this.state.renkoTimestamps = res[1];
        } else {
          candles = Utils.ticksToCandles(ticks, this.state.timeframe);
        }

        let relData = []

        if(candles) { // && this.showRelative
          let min = response.data.extra.bid.min * 0.99
          let max = response.data.extra.bid.max * 1.01
          console.log(min, max)
          let range = max - min

          relData = _.map(candles, function(o) {
            return [
                o[0],
                round((((o[1]-min) / range) * 100.0), 1),
                round((((o[2]-min) / range) * 100.0), 1),
                round((((o[3]-min) / range) * 100.0), 1),
                round((((o[4]-min) / range) * 100.0), 1)
              ]}
          )
        }

        this.setState({ data: candles, relData: relData })
      })
      .catch(error => {
        console.log(error);
      });
  }

  predictClick() {
    console.log('predict')
  }

  render() {
    var relativeChart;
    var renkoTimetampChart;
    var timeframePrefix;
    var timeframeSuffix;
    const columns = [{ key: 'id', name: 'ID' }, { key: 'tstamp', name: 'Timestamp' }];
    const rows = this.state.renkoTimestamps;
    const rowGetter = rowNumber => rows[rowNumber];

    if (this.state.showRelative) {
      relativeChart = <Chart data={this.state.relData}/>
    }

    if (this.state.renkoChart)
    {
      timeframePrefix = <p>Box Size:&nbsp;</p>
      timeframeSuffix = <p>&nbsp;USD</p>

      if (rows.length > 0) {
        renkoTimetampChart = <ReactDataGrid
                              columns={columns}
                              rowGetter={rowGetter}
                              rowsCount={rows.length}
                              minHeight={250} />
      }
    } else {
      timeframePrefix = <p>TimeFrame:&nbsp;</p>
      timeframeSuffix = <p>&nbsp;Minutes</p>
    }

    return(
      <div>
        <h1>Tick Data Visualizer</h1>
        <div>
          <div className="line-container">
            <p>Start Time:&nbsp;</p>
          </div>
          <div className="line-container">
              <DatePicker
                  selected={this.state.startDate}
                  onChange={this.handleStartDateChange}
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={15}
                  dateFormat="LLL"
              />
          </div>
        </div>
        <div>
          <div className="line-container">
            <p>End Time:&nbsp;</p>
          </div>
          <div className="line-container">
            <DatePicker
                selected={this.state.endDate}
                onChange={this.handleEndDateChange}
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={15}
                dateFormat="LLL"
            />
          </div>
        </div>
        <div>
          <div className="line-container">
            { timeframePrefix }
          </div>
          <div className="line-container">
            <input
              type="text"
              value={this.state.timeframe}
              onChange={this.handleTimeframeChange}>
            </input>
          </div>
          <div className="line-container">
            { timeframeSuffix }
          </div>
        </div>
        <div>
          <div className="line-container">
            <p>Renko Chart: </p>
          </div>
          <div className="line-container">
            <input
               name="renkoChart"
               type="checkbox"
               checked={this.state.renkoChart}
               onChange={this.handleCheckboxChange}
             />
          </div>
        </div>
        <div>
          <div className="line-container">
            <p>Show Relative Candles: </p>
          </div>
          <div className="line-container">
            <input
               name="showRelative"
               type="checkbox"
               checked={this.state.showRelative}
               onChange={this.handleCheckboxChange}
             />
          </div>
        </div>
        <Chart data={this.state.data}/>
        { relativeChart }
        { renkoTimetampChart }

        <Button value='Render' clickHandler={this.renderClick} />
        <Button value='Predict' clickHandler={this.predictClick} />
    </div>
    )
  }
}

export default Home
