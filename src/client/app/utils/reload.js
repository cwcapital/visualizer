export default function reload(module) {
    delete require.cache[require.resolve(module)]
    return require(module)
}
