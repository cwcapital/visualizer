import 'react-hot-loader/patch';
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from "react-router-dom"
import App from './components/App.js';
import { AppContainer } from 'react-hot-loader';

const _render = Component => {
  render(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
    document.getElementById('app'));
}

_render(App);

if (module.hot) {
   module.hot.accept('./components/App.js', () => {
     _render(App);
   })
 }
